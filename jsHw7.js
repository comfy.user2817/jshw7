

"use strict"



//1

function isPalindrome(str) {
    
    let check = '';
    
    for (let i = str.length-1; i >= 0; --i) {
        check += str[i];
    }
    
    return str == check;
}


//Або 


function isPalindromeTwo(str) {
    
    return str.split('').reverse().join('') == str;
}




//2


function checkString(str,max) {
    return str.length <= max;
}



//3


function yourAge() {
    let age = prompt("ВВедіть ваш вік", "2000-01-01");
    let now = new Date();
    let trueAge = Math.floor((now - new Date(age))/(1000*60*60*24*365));
    return alert(`Ваш вік складає ${(trueAge)} років`);
    
    
}
